package com.aa.sampleapp.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

object DataBindingFunctions {

    //Load image urls with data binding and picasso from xml
    @JvmStatic
    @BindingAdapter("image_url")
    fun setImageUrl(imageView: ImageView, url: String) {
        Picasso.get().load(url).into(imageView)
    }
}