package com.aa.sampleapp.models

data class ImageModel(
    val albumId: Int?,
    val id: Int?,
    val thumbnailUrl: String?,
    val title: String?,
    val url: String?
)