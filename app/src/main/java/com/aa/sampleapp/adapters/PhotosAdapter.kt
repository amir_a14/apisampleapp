package com.aa.sampleapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aa.sampleapp.databinding.ItemImageBinding
import com.aa.sampleapp.models.ImageModel
import kotlinx.android.synthetic.main.item_image.view.*


class PhotosAdapter(private val images: ArrayList<ImageModel>) :
    RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        //Provide data binding
        val itemImageBinding =
            ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotosViewHolder(itemImageBinding)
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        holder.bind(images[position])

        //This line is need to scroll long texts (marquee)
        holder.itemView.title.isSelected = true
    }

    override fun getItemCount(): Int = images.size

    class PhotosViewHolder(private val imageBinding: ItemImageBinding) :
        RecyclerView.ViewHolder(imageBinding.root) {

        //Bind view with data binding
        fun bind(imageModel: ImageModel?) {
            imageBinding.setImage(imageModel)
            imageBinding.executePendingBindings()
        }
    }
}