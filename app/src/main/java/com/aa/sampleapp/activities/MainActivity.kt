package com.aa.sampleapp.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.aa.sampleapp.R
import com.aa.sampleapp.adapters.PhotosAdapter
import com.aa.sampleapp.databinding.ActivityMainBinding
import com.aa.sampleapp.viewmodels.MainActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var viewModel: MainActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Provide ViewModel
        viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

        //Provide DataBinding
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel
        //This line is need for live data to change views with data binding
        binding.lifecycleOwner = this

        //Observe server response
        viewModel.photos.observe(this, {
            if (it != null) {
                mainRecyclerView.layoutManager = GridLayoutManager(this, 2)
                mainRecyclerView.adapter = PhotosAdapter(it)
            } else {
                //Clear recycler view when data is null
                mainRecyclerView.adapter = null
            }
        })

        //Get data from server when main activity is open
        viewModel.getData()
    }
}