package com.aa.sampleapp.remote

import com.aa.sampleapp.models.ImageModel
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiInterface {
    @GET("/photos")
    //Using RXJava observable instead of retrofit call
    fun getPhotos(): Observable<ArrayList<ImageModel>>
}