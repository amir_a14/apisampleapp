package com.aa.sampleapp.viewmodels

import android.app.Application
import android.widget.Toast
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.aa.sampleapp.models.ImageModel
import com.aa.sampleapp.remote.ApiInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivityViewModel @ViewModelInject constructor(
    application: Application,
    var apiInterface: ApiInterface
) :
    AndroidViewModel(application) {

    val isLoading: MutableLiveData<Boolean> by lazy { MutableLiveData(false) }

    //Store server response into liveData
    val photos: MutableLiveData<ArrayList<ImageModel>> by lazy { MutableLiveData() }

    /**
     * Getting photos from server and toast error when it's failed
     */
    fun getData() {
        photos.value = null
        isLoading.value = true
        val dispose = apiInterface.getPhotos().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //Success
                isLoading.value = false
                photos.value = it
            }, {
                //Failed
                isLoading.value = false
                Toast.makeText(getApplication(), it.message, Toast.LENGTH_LONG).show()
            })
    }
}